#include <Adafruit_NeoPixel.h>
#define LEDPIN 6 // connect the Data from the strip to this pin on the Arduino
#define NUMBER_PIXELS 300 // the number of pixels in your LED strip
#define ARRAY_SIZE 11
//TODO vl NEO_RGBW
Adafruit_NeoPixel strip           = Adafruit_NeoPixel(NUMBER_PIXELS, LEDPIN, NEO_GRB + NEO_KHZ800);
const uint8_t  ledsOn             = 25;
const uint32_t violet             = strip.Color(200, 0, 200);
const uint32_t yellow             = strip.Color(200, 200, 0);
const uint32_t cyan               = strip.Color(0, 200, 50);
const uint32_t lightBlue          = strip.Color(0, 115, 230);
const uint32_t green              = strip.Color(0, 200, 34);
const uint32_t red                = strip.Color(153, 0, 0);
const uint32_t violetLight        = strip.Color(204, 0, 102);
const uint32_t blue               = strip.Color(6, 0, 196);
const uint32_t orange             = strip.Color(165, 85, 0);
const uint32_t white              = strip.Color(100, 100, 100);
const uint32_t greenish        = strip.Color(0, 230, 115);
const uint32_t off                = strip.Color(0, 0, 0);


const uint32_t colors[ARRAY_SIZE] = {violet, yellow, cyan, violetLight, green, red, orange, blue, white, greenish,lightBlue};

const uint16_t pixels             = NUMBER_PIXELS;
const int waitTime                = 10;
const int wait                    = 20;

//Button stuff not connected, yet
const int buttonPin = 2;
const int buttonPinParty = 3;
int buttonState = 0;
int buttonStateParty = 0;

//CURRENT COLORS
/////////////////////

uint32_t color1;
uint32_t color2;
uint32_t color3;
uint32_t color4;
//////////////


enum Vibe {
  party,
  chill
};




// amount ist wie viele auf einmal aktiviert werden sollen
void turnOnLedsRainbow(int index,int ledAtOneTime) {
  int i = 0;
  while (i < ledAtOneTime) {
    uint32_t color = colors[random(0,ARRAY_SIZE)];
    strip.setPixelColor(index++, color);
    i++;
    if ((index + ledAtOneTime) >= pixels) {
      strip.setPixelColor(((index + ledAtOneTime) - pixels), colors[random(0,ARRAY_SIZE)]);
    }
  }
}


void loopingThroughRain() {
  for (int i = 0; i < pixels; i++) {
    // TODO when last led is reached go to the first one (durchgehend)
    turnOnLedsRainbow(i, ledsOn);
    strip.show();
    turnOffLeds(i);
  }
  delay(wait);
}

void loopingThrough() {
    Serial.print("meteor");
  uint32_t color = colors[random(0,ARRAY_SIZE)];
  int ledAtOneTime = 20;
  for (int i = 0; i < pixels; i++) {
    turnOnLeds(i, color, ledAtOneTime);
    strip.show();
    if (i > 0) {
      turnOffLeds(i);
    }
  }
}

void loopingThroughRainbow( int ledAtOneTime) {
    Serial.print("looping rainbow");
  int u = 0;
  int v = 0;
  for (int i = 0; i < pixels; i++) {
    while (u < 160) {
      turnOnLedsRainbow(i + u, ledAtOneTime);
      u = u + 10;
    }
    turnOnLedsRainbow(0,ledAtOneTime);
    strip.show();
    if (i > 0) {
      turnOffLeds(ledAtOneTime);
      while (v < 160) {
        turnOffLeds(i + v);
        v = v + 10;
      }
    }
    strip.show();
    delay(wait);

  }
}

void loopingBack() {
    Serial.print("Looping back");
  for(int i =0; i < ARRAY_SIZE; i++){
    uint32_t color= colors[random(0,ARRAY_SIZE)];
    for (int i = pixels; i >= 0; i--) {
      turnOnLedsBack(i, color, ledsOn);
      strip.show();
      turnOffLedsBack(i, ledsOn);
      strip.show();
      delay(wait);
    }
  }
}


void blinkColorsAllSameTime(uint32_t color) {
  for (int i = 0; i < ARRAY_SIZE; i++) {
    setAll(colors[random(0,ARRAY_SIZE)]);
    //uint8_t as para from 0 to 255
    strip.setBrightness(150);
    strip.show();
    delay(2500); //show color for 2 sec
  }
  strip.setBrightness(255);
}


//Abstand zwischen start und finish sollte nichz zu groß sein, da es sonst zur überhitzung kommt
void fadeInAndOut() {
    Serial.print("Fade in out");
  uint32_t color = colors[random(0,ARRAY_SIZE)];
  uint8_t red = getRed(color);
  uint8_t green = getGreen(color);
  uint8_t blue = getBlue(color);

  for (uint8_t b = 0; b < 255; b++) {
    for (int i = 0; i < pixels; i++) {
      strip.setPixelColor(i, red * b / 255, green * b / 255, blue * b / 255);
    }
    strip.show();
    delay(wait);
  };
  for (uint8_t b = 255; b > 0; b--) {
    for (int i = 0; i < pixels; i++) {
      strip.setPixelColor(i, red * b / 255, green * b / 255, blue * b / 255);
    }
    strip.show();
    delay(waitTime);
  };
};

void flash() {
    Serial.print("Flash");
  long timePassed = millis();
  const long time = timePassed+5000;
  while (timePassed < time) {
    uint32_t color = colors[random(0,ARRAY_SIZE)];
    setAll(color);
    strip.show();
    delay(250);
    setAll(off);
    strip.show();
    delay(250);
    timePassed = millis();
  }

}

void twinkle() {
Serial.print("Twinkle");
    for(int i =0; i < ARRAY_SIZE; i++){
        long timePassed = millis();
      const long time = timePassed+5000;
      uint32_t color= colors[random(0,ARRAY_SIZE)];
      while (timePassed < time) {
        int pixel = random(pixels);
        strip.setPixelColor(pixel, color);
        strip.show();
        strip.setPixelColor(pixel, off);
        delay(5);
        strip.show();
            timePassed = millis();
      }
    }
}



void turnOnLedsBack(int index, uint32_t color, int amount) {
  int i = 0;
  while (i < amount) {
    strip.setPixelColor(index--, color);
    i++;
  }
}

void turnOffLedsBack(int index, int amount) {
  int i = 0;
  while (i < amount) {
    turnOffLed(index++);
    i++;
  }
}

void turnOnLeds(int index, uint32_t color, int ledsOnAtOneTime) {
  int i = 0;
  while (i < ledsOnAtOneTime) {
    strip.setPixelColor(index++, color); //index++
    i++;
  }
  strip.show();
}

void turnOffLeds(int index) {
  int i = 0;
  while (i < 25) {
    turnOffLed(index--);
    i++;
  }
  strip.show();
}

void turnOnLed(int index, uint32_t color) {
  strip.setPixelColor(index, color);
}

void turnOffLed(int index) {
  strip.setPixelColor(index, off);
}

void setAll(uint32_t color) {
  for (int i = 0; i < pixels; i++) {
    strip.setPixelColor(i, color);
  }
  strip.show();
}



//////////////////////////////////// meteorRain(0xff,0xff,0xff,10, 64, true, 30);
void meteorRain() {
  Serial.print("meteor");
  uint32_t color = colors[random(0,ARRAY_SIZE)];
  setAll(color);
  byte meteorSize = 10;
  byte meteorTrailDecay = 64;
  boolean meteorRandomDecay = true;
  int SpeedDelay = 30;

  for (int i = 0; i < pixels * 2; i++) {
    // fade brightness all LEDs one step
    for (int j = 0; j < pixels; j++) {
      if ( (!meteorRandomDecay) || (random(10) > 5) ) {
        fadeToBlack(j, meteorTrailDecay );
      }
    }
    // draw meteor
    for (int j = 0; j < meteorSize; j++) {
      if ( ( i - j < pixels) && (i - j >= 0) ) {
        strip.setPixelColor(i - j, color);
      }
    }
    strip.show();
    delay(SpeedDelay);
  }
}

void fadeToBlack(int ledNo, byte fadeValue) {
#ifdef ADAFRUIT_NEOPIXEL_H
  // NeoPixel
  uint32_t oldColor;
  uint8_t r, g, b;
  int value;

  oldColor = strip.getPixelColor(ledNo);
  r = (oldColor & 0x00ff0000UL) >> 16;
  g = (oldColor & 0x0000ff00UL) >> 8;
  b = (oldColor & 0x000000ffUL);

  r = (r <= 10) ? 0 : (int) r - (r * fadeValue / 256);
  g = (g <= 10) ? 0 : (int) g - (g * fadeValue / 256);
  b = (b <= 10) ? 0 : (int) b - (b * fadeValue / 256);

  strip.setPixelColor(ledNo, r, g, b);
#endif
#ifndef ADAFRUIT_NEOPIXEL_H
  // FastLED
  leds[ledNo].fadeToBlackBy( fadeValue );
#endif
}

uint8_t getRed(uint32_t color) {
  return (color >> 16) & 0xFF;
}
uint8_t getGreen(uint32_t color) {
  return (color >> 8) & 0xFF;
}
uint8_t getBlue(uint32_t color) {
  return color & 0xFF;
}
///////////////////////////////////////////////////////

void twoSidesAnimated() {
  int max = ARRAY_SIZE;
  for(int i =0; i< ARRAY_SIZE;i++){
    int firstValue =rand()%(max + 1);
    int secondValue= rand()%(max+ 1);
    int thirdValue =rand()%(max + 1);
    int fourthValue= rand()%(max+ 1);
    if(firstValue == secondValue || firstValue == thirdValue){
      firstValue =rand()%(max + 1);
    }else if(secondValue == fourthValue){
      secondValue = rand()%(max + 1);
    }
    twoSide(colors[firstValue], colors[secondValue]);
    delay(200);
    twoSideAni(colors[thirdValue], colors[fourthValue]);
    twoSideAniRev(colors[firstValue], colors[secondValue]);
  }
}

void twoSide(uint32_t colorLeft, uint32_t colorRight) {
  setPixelsColorInRange(0, (NUMBER_PIXELS / 2), colorLeft);
  setPixelsColorInRange((NUMBER_PIXELS / 2), NUMBER_PIXELS, colorRight);
}

void twoSideAni(uint32_t colorLeft, uint32_t colorRight) {
  setPixelsColorInRangeAni(0, NUMBER_PIXELS / 2, colorLeft);
}

void twoSideAniRev(uint32_t colorLeft, uint32_t colorRight) {
  setPixelsColorInRangeAniRev(0, NUMBER_PIXELS / 2, colorLeft);
}

void setPixelsColorInRange(int rangeLow, int rangeHigh , uint32_t color) {
  int i = rangeLow;
  while (i < rangeHigh) {
    strip.setPixelColor(i++, color);
  }
  strip.show();
}

void setPixelsColorInRangeAni(int rangeLow, int rangeHigh , uint32_t color) {
  int i = rangeLow;
  int j = (NUMBER_PIXELS-1);
  while (i < rangeHigh) {
    strip.setPixelColor(i++, color);
    strip.setPixelColor(j--, color);
    
    strip.show();
    delay(200);
  }

}

void setPixelsColorInRangeAniRev(int rangeLow, int rangeHigh , uint32_t color) {
  int i = rangeHigh;
  int j = rangeHigh;
  while (i >= rangeLow) {
    strip.setPixelColor(--i, colors[0]);
    strip.setPixelColor(j++, colors[7]);
    strip.show();
    delay(200);
  }
}
/////////////////////////////////////////////////////////////////
void modTwo(uint32_t firstColor, uint32_t secondColor){
  int i = 0;
  while(i <= NUMBER_PIXELS){
    if(i % 2){
      strip.setPixelColor(i++, firstColor);
    }else{
      strip.setPixelColor(i++, secondColor);
    }
    strip.show();
    delay(50);
  }
}

void modTwoRev(uint32_t firstColor, uint32_t secondColor){
  int i = NUMBER_PIXELS;
  while(i >= 0){
    if(i % 2){
      strip.setPixelColor(i--, firstColor);
    }else{
      strip.setPixelColor(i--, secondColor);
    }
    strip.show();
    delay(50);
  }
}

void mod(){
  Serial.print("mod");
  modTwo(color1, color2);
  modTwoRev(color3,color4);
}

void (*twinkles)() = twinkle;
void (*twoSidesAnimated1)() = twoSidesAnimated;
void (*loopingBack1)() = loopingBack;
void (*loopingThroughRain1)() = loopingThroughRain;
void (*mod1)() = mod;
void (*flash1)() = flash;
void (*loopingThrough1)() = loopingThrough;
void (*fadeInAndOut1)() = fadeInAndOut;
void (*meteorRain1)() = meteorRain;

void (* programs [])(void) = {twinkles,twoSidesAnimated1,loopingBack1,loopingThroughRain1,
                             mod1,flash1,loopingThrough1,fadeInAndOut1,meteorRain1};

void setup() {
  Serial.begin(9600);
  pinMode(buttonPin, INPUT);
  pinMode(buttonPinParty, INPUT);
  strip.begin();
  strip.show();
  // strip.setPixelColor(5,blue);
  //strip.show();
  randomSeed(analogRead(0));
}

void loop() {
  color1=  colors[random(0,ARRAY_SIZE)];
      color2=  colors[random(0,ARRAY_SIZE)];
      color3=  colors[random(0,ARRAY_SIZE)];
      color4=  colors[random(0,ARRAY_SIZE)];
 /* blinkColorsAllSameTime(1500);
    twinkle();
    twoSidesAnimated();
    loopingThroughRain();
    loopingBack();
    for(int i =0; i < ARRAY_SIZE; i++){
      color1=  colors[random(0,ARRAY_SIZE)];
      color2=  colors[random(0,ARRAY_SIZE)];
      color3=  colors[random(0,ARRAY_SIZE)];
      color4=  colors[random(0,ARRAY_SIZE)];
      
      mod();
      flash();
      loopingThrough();
      fadeInAndOut();
      meteorRain();
    }

  */
   programs[random(0,9)]();
  
//flash();
  //!!!!!!!!!!!!!!!!!!!!!!!!!DONT REMOVE
  //partyVibe[0](red,5);
  //delay(10);
  //meteorRain(cyan, 10, 64, true, 30);
  /*delay(10);
    setAll(off);
    delay(1000);
    setAll(violetLight);
    delay(500);
    setAll(off);*/
  //runnignLights(white,100);
  //delay(10);
}
